const express = require("express");
const app = express();
const session = require('express-session')
const fs = require("mz/fs");

app.set("view engine", "ejs")
app.use(express.static("static"))
app.use(session({
    secret: 'keyboard cat eats keyboards',
    resave: false,
    saveUninitialized: true
}))

async function loadSongTree() {
    var music = fs.readdir("./static/music/");
    var genres = JSON.parse(await fs.readFile("./static/music/genres.json", "utf-8"));
    return genres;
}

app.get("/", (req, res) => res.render("pages/index"))
app.get("/startgame/1", (req, res) => 
    loadSongTree().then(st => res.render("pages/startgame/1", {songTree: st})))

app.listen("7777")